package net.chenlin.dp.ids.common.exception;

/**
 * passport自定义异常
 * @author zcl<yczclcn@163.com>
 */
public class PassportException extends RuntimeException {

    private static final long serialVersionUID = 5273770483221410239L;

    public PassportException(String msg) {
        super(msg);
    }

    public PassportException(String msg, Throwable throwable) {
        super(msg, throwable);
    }

    public PassportException(Throwable throwable) {
        super(throwable);
    }

}
